<?php

use Illuminate\Http\Request;
use App\Product;

Route::get('/indicio',function(){
    return "Probando aplicación laravel";
});

// Route::get('/products',function(){
    
//     $products = Product::orderBy('created_at', 'desc')->get();

//     return view('products.index', compact('products'));
// })->name('products.index');

Route::get('/products',function(){
    
    $products = Product::orderBy('created_at', 'desc')->get();

    return response()->json([
        'response' => $products
    ]);
});

Route::get('/products/create',function(){
    return view('products.nuevoProdcuto');
})->name('products.name');

Route::post('/products',function(Request $request){
    // return $request->all();
    $newProduct = new Product;
    $newProduct -> description = $request -> input('description');
    $newProduct -> price = $request -> input('precio');

    $newProduct->save();

    return redirect()->route('products.index')->with('info',"Se guardó con éxito");

})->name('products.store');